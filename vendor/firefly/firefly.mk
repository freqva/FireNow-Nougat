CUR_PATH := vendor/firefly/

PRODUCT_PACKAGES += \
    WallpaperPicker	\
    firefly_sdkapi_demo \
	Antutufull

ifneq ($(filter rk3399_firefly_aio_box rk3399_firefly_aio_lvds_box rk3399_firefly_aio_edp_box rk3399_firefly_aio_mipi_box, $(strip $(TARGET_PRODUCT))), )
PRODUCT_PACKAGES += \
    HDMIIn
endif

include $(CUR_PATH)/cat_log/cat_log.mk
include $(CUR_PATH)/usb_mode_switch/usb_mode_switch.mk
include $(CUR_PATH)/iperf/iperf.mk



PRODUCT_COPY_FILES += \
	$(CUR_PATH)/fireflyapi/fireflyapi.jar:system/framework/fireflyapi.jar \
	$(CUR_PATH)/fireflyapi/fireflyapi:system/bin/fireflyapi 
